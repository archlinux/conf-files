# Rolling your own security team for fun and no profit at all - Levente 'anthraxx' Polyak

* Has Arch considered becoming a CVE Numbering Authority? - foxboron
* Not all software projects issue CVEs for potentially security-breaking bugs. Do the Arch maintainers ever manually review code? Is security a factor in deciding whether to package some software? - tamz

* How do closed-source proprietary packages in [community] work? (i.e. discord) Are there any special procedures with respect to security/trust/distribution for these packages? - danielrparks
* What does it take for a proprietary package to get included in the official repos? - danielrparks

* is there a fixed rule when no ASA is done? You mentioned 6 months but that sounded a bit like an example - u1106

* What software do you use for the security tracking workflow? - oak__

* Is it part of the security team's mandate to verify the security fixes or is that on the individual package maintainers responsibility? - alkazar79

* Does Arch Linux have any paid staff? if yes, how many? And where does the money come from? - Hoite

* Concerning arch infrastructures (mirrors, archlinux.org, etc). How is the security handled in there? Pentests?, Vulnerability assessment? - ArchGuest

* How to get involved with the Arch Linux security team? - diabonas

* Can you talk a bit more about the API for the security tracker? Where is it? Is it publicly available or only for Arch Security Team? - ArchConf|53

* How do you identify burnout? 
I feel that I have that intrinsic motivation, especially about optimizing things, but I don't know how to make it sustainable or whether my motivation is sustainable or not. - Codinaut
* If you have intrinsic motivation, but seems your team doesn't. How do you keep that situation sustainable? If you want to talk to your team about something, but no one is interested, it would be quite hard to keep it "on fire" - Codinaut

* What are typical vulnerabilities (and what are their sources - new users or everyone?)? - ynikitenko
