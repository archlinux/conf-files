# GamerOS: An Arch Linux based gaming OS - Alesh Slovak

* Are you actually allowed to ship Steam pre-installed? - svenstaro

* Why not just pacman and btrfs snapshots? What's the advantage of your method? - svenstaro

* What do you do about games that required to be installed on an ext4 partition? - dvzrv

* What if a config file on the overlay filesystems needs an update? - JerryXiao

* Are or willl you use delta updates for image updates? -Ilgaz

* So ostree is a overlay filesystem that you can apply to your current filesystem to have "stable" updates ? I am guessing uses diffs or something to reduce disk usage? - ebal

* How is compatibility between GamerOS and a Steam Link. Steam Link with Steam on my existing Arch linux OS has been hit and miss. - aurroq

* How well does Steam Remote Play work? What kind of specs are needed for that? - Ram-z

* What has been the biggest challenge when adapting steamOS to Arch systems? - kieto

* Is it only available for linux-compatible games or I can also play Battle.Net games? - rotsix

* Can I install GOG games on your system? - ynikitenko

* Is there any Kodi integration in GamerOS? - Ram-Z

* Does it have any benefits in the game compatibility department compared to a standard install of Arch Linux? - rhysperry111

* Any plans to support wireless during install? - lurst

* Will the image files contain all possible emulators inside of it? Or is the plan to reinstall them after every update? - kieto

* Was the distro ever noticed by any big Windows only game development companies? - Ilgaz

* Did any company apporach you to make a gaming "console" based on your OS? Or making a huge donation and shipping a commercial thing based on it? GPL of course and if so, would you considering selling the distro to them, if you get a lot of money or some money or not at all? - Ilgaz

* Suggested reword: Have you considered any offers for selling this distro, and if so, would you keep it GPL?

* Any tweaks Steam-like style for Epic Games Store ones? - thaewrapt

* How much of the gamer os release process is automated? - svenstaro

* What percentage of steam games can run on linux with proton now? - ungages