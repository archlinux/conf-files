# Protecting secrets and securing the boot process using a Trusted Platform Module (TPM) - Jonas Witschel

* How do i use when i have several computers? - oak__

* What's the advantage of using TPM for storing keys as opposed to storing keys on an encrypted disk? - jonathon
* Is it possible to use TPM to store disk encryption keys? - ArchGuest

* Can you do secureboot with tpm? Also, can it be prevented from being rewritten by archiso(usb)? - j4nj0n
* It seems to me that "trusted boot" overlays with "secureboot" in some ways? - hexchain

* Do you not actually need to type a passphrase one boot time if you set it up with tpm? - lurst

* How does TOTP actually derive a code from the key and time. Is it something as simple as hash(key + time)? - rhysperry111

* Nice talk Jonas! But how do you account for time differences between your phone's clock and your computer's clock? Some times tpm2-totp output will take more than 30 seconds to display the right token. - grazzolni

* If your system is compromised, that attacker cannot steal your key, fine. But they can use the TPM (and your keys) as long as your machine is running. Many machines are always running. So what is the benefit in the end? - u1106

* Was there any research into the physical security of TPMs? Would stealing a TPM mean stealing the keys inside? - _zirak_

* If i use tpm disk encryption. dual-boot with windows can affect the booting or they are independent. - alexEP

* Is the hoodie good quality? Thinking about getting one - rhysperry111