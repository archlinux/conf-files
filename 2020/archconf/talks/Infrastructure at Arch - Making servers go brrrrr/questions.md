# Infrastructure at Arch - Making servers go brrrrr - Sven-Hendrik Haase

* Is the Arch infrastructure running on Arch? - entire community lol

* How do you handle server maintenance? Updates etc. - mackilanu

* Pros/cons of bare metal servers? - mackilanu

* How do you handle secrets in the infrastructure? For Ansible im guessing ansible vault? any interesting security practices? - pitastrudl

* What made you choose Hetzner to host your services rather than another cloud provider? - dr_hashimoto

* What's the bus factor now? - lurst

* do you encrypt disks on infra servers? - pfactum

* How do you manage server access when running Ansible roles, logging in, etc. Do you auth directl to servers with your own keys or auth to some runner which in turn uses its key? - the_wizzrd

* I am guessing you are using VMs (vps) instead of dedicated boxes. So are you using packer to build immutable images based on archlinux ?  So when you update/upgrade these images? Every week , every month ? is there any CI/CD automation for that? - ebal

* how do you handle security of the servers? i mean hardening, managing roles/accounts, etc - Archguest

* What about disk encryption for the cloud servers? - fernaovellozo

* Do you use lxc/lxd containers for anything - kieto

* in making an arch server, do i install linux-lts or linux-hardened?

* are they pentests organized on the infrastructures or  any security-related like nikto/nessus/* stuffs? - ArchGuest

* Do you use docker containers for anything? - lurst

* Are the grafana dashboards in a repo somewhere? - kgz

* Will the already-created forum and aur accounts get merged to SSO? - Archguest

* Is GitHub used for anything regarding to DevOps? Or is it just a place to mirror SVN packages and other stuff? - orhun

* Are you thinking of Switching the forum over to Discourse https://www.discourse.org/ or something? - bittin

* What's the deal with BBS and PHP? Could you please elaborate? - fernaovellozo

* Do any of your services need a prometheus exporter writing?  -kgz

* Who monitors/advises regarding GDPR compliance? - WinterStar

* Any plans of switching out the current secrets storage with Hashicorp's Vault?, Given that your are using Terraform more and more. - Nightf0x3r

* I'd like to know if there's going to be a support group on any messenger? telegram would be a good choice - swodig112

* Is there plans to migrate the bug tracker to Gitlab too - RamZ
