# How to organise your digital life in a privacy-preserving, machine-agnostic, and practical manner

* I've never heard of mailbox, but no mention of protonmail or tutanota? They also support pgp - pitastrudl

* Did you look into the differences between mailbox.org and protonmail? -steamwreck

* Why do you prefer to use mailbox.org for email hosting instead of running your own server? -steamwreck

* Have you tried Gitlab for projects and trello-like boards? - Ram-Z

* Ever considered a plain old folder with rsync or unison? - abaumann

* Have you looked into TMSU? It enables you to tag files - nfraprado

* Why not hosting nextcloud on a vps? That's pretty simple now with these docker things - rotsix

* Have you thought about or tried a self-hosted nextcloud instance? There you have cloud storage, calendars, contacts and tasks already - kieto

* What protocols does rclone use? - aminvakil

* Is it possible to DIY yubikey? is yubikey a standard phsyical token? or can someone do their own? - pitastrudl

* What's next for you for your system? What do you think about doing next, or what doesn't currently spark joy? _zirak_

