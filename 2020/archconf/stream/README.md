Arch Conf 2020 OBS Setup
========================

Import scenes ./ArchConf.json through Profile and Scene Collection.

# Scene Switcher and Break

## Scene Switcher
Download: 
https://github.com/WarmUpTill/SceneSwitcher/releases

Unpack into `/usr/lib/obs-plugins/`

Tools -> Advanced Scene Switcher.

Head to sequence, and "Load Rround Trips from file". The file is ./SceneSwitcher.txt

Important to note you can keep it playing and it won't interrupt the talks.

## Spectralizer

Download: 
https://github.com/univrsal/spectralizer

Unpack into `/usr/lib/obs-plugins/`

Sources -> Spectralizer.Source

## Load info on `TalkSchedule` and `TalkDetail`

`cd ./sceneDetails/ && make loop`

# Streaming

The stream was:

    Local OBS -> RTMP -> C3VOC -> Twitch (restream)

https://c3voc.de/wiki/distributed-conference

c3voc provided relive and restream for us, and was coordinated in `#voc-lounge` @ hackint

# Music

Herris Heller - In 4K.
Playlist: https://open.spotify.com/playlist/7AtgguxCr9jFt5vmKKXAkt?si=b1A4BbynSJK3zRsoX-Qftw

* Dreary Summer
* Blue Yellow Sky
* Because You Asked
* Day You left
* Gazes
* Because Rain
* When You Were Here
* Mr.Melatonin
* Trendy
* Jacket in the Summer
* Outside
* Frontier
* Ocean Scent
* Forget the Past

# DJ Set - VJ Loop

Mike Winkelman
Licensed under Creative Commons
https://www.beeple-crap.com/vjloops

* Fiber Optical: https://vimeo.com/238083470
* Hexxx: https://vimeo.com/202376104
* Pray State: https://vimeo.com/271119904
* Cleanroom: https://vimeo.com/216168912
