# Archiso - creating an installation medium - David Runge

* Why [host the archiso project on GitLab instead of] GitHub? - Energicreator

* Were there any specific reasons why GPLv3-or-later was chosen for archiso? wouldn't something like MIT style encourage more adoption? - aratiu4

* Will archiso stay shell or do you have any rough plans to change that? - svenstaro

* How is it determined what software is included in the iso? mackilanu

* How can I install run_archiso? - lurst

* Are there any plans/ideas to run automated installation profiles (e.g. full-disk encryption, lvmraid, ...) in CI to make sure not only the generated ISO is correct, but the included software works as expected, too? - diabonas

* How it goes with regard to dracut and Red Hat collaboration? any help needed (i'm a RH employee)? do you need any help? - pfactum
* Will you ever look at dracut? it's creates mine initrds with disk encryption support quite well" - j1rjyuhw9gdz9nkg8zj

* Did I miss where archlinux-bootstrap.tar comes from? Is it the same project/package or something unrelated? - u1106

* is there a way to run scripts automatically? - oak_

* Does anybody use/test PXE-booting of archiso? - demaio

* Is it a good time to rebase on latest archiso, or are there still a lot of disruptive changes coming soon? - alkazar79

* Archiso recently adds a service called reflector which connects to the internet and sorts mirrors by speed without user permission. Is it against the KISS principle? - JerryXiao

* Making an archiso releng causes a power outage (most of the time mid-making SquashFS), what to do? - concretepad

* Have you looked into u-root for initrd generation? - cyrevolt 

* Where are the zsh profiles stores (for the default installation) - oak_ -- answered on irc 

* will they ever disable those fimware warnings in mkinitcpio? - rhysperry111