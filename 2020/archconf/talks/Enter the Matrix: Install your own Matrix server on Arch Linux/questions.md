# Enter the Matrix: Install your own Matrix server on Arch Linux - Brendan Abolivier

## Future

* When will dendrite be ready? - daurnimator

* Also are identities portable yet? that was the 2nd factor that made me not want to experiment with my own homeserver - daurnimator

## Installation

* How do you think the packaging and first steps after installation of synapse can be improved -dvzrv (David)
* How would a "best practices" install differ from this demo? - Twitch

## Database

* Is Matrix tested with alternative postgres implementations like CockroachDB or YugaByteDB? - sateffen
* Is it possible to implement a NoSQL database? - mackilanu

* What's the current state of matrix-appservice-irc? - berberman

## Usage

* Is matrix a viable replacement for IRC these days? - IRC

* What do you suggest regarding to a more lightweight matrix client? is there any? - orhun