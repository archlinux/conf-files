-> Packaging Rust Applications for Arch Linux <-
=========

-> Orhun Parmaksız <-

-------------------------------------------------

-> # Hmm, orhun? <-

~ FOSS Enthusiast.
~ Rust Developer.
~ Full Time Learner.

-> * https://orhun.dev/

-------------------------------------------------

-> # Rust? <-

-> *Rust* is a multi-paradigm programming language <-
-> focused on performance and safety, <-
-> especially safe concurrency. <-

-> Rust is syntactically similar to C++, <-
-> and provides memory safety <-
-> without using garbage collection. <-

-> ## https://en.wikipedia.org/wiki/Rust_(programming_language) <-

-------------------------------------------------

-> # Why Rust? <-

* Performance
  * Fast & Efficient
* Reliability
  * Memory Safety
  * Error Handling
* Productivity
  * General Purpose
  * Project Oriented
  * Well Supported

-> ## https://www.rust-lang.org/ <-

-------------------------------------------------

-> # Applications <-

* Command Line
* WebAssembly
* Networking
* Embedded

-> ## https://www.rust-lang.org/ <-

-------------------------------------------------

-> # Linux? <-

* Robust configuration
* Automated manuals
* Flexible logging
* Easy distribution
* Communication with machine 

-> "Expect the output of every program to <-
-> become the input to another, as yet unknown, program." <-

-> ## https://www.rust-lang.org/what/cli <-

-------------------------------------------------

-> # Rewrite it in Rust! <-

-------------------------------------------------

-> # Packaging demo <-

* https://wiki.archlinux.org/index.php
  * /PKGBUILD
  * /Rust_package_guidelines
  * /VCS_package_guidelines
  * /DeveloperWiki:Building_in_a_clean_chroot
  * /AUR_submission_guidelines

-------------------------------------------------

-> ## End-of-presentation <-

-> GitHub: orhun
-> Twitter: orhunp_
-> IRC: freenode::orhun

-> * https://orhun.dev/
