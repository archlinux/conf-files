# The State of Reproducible Builds - Morten Linderud

* Why do the man pages "need" the timestamp? - lurst

* How are those two enviroments set up? What are you using to keep them isolated from each other and the host?  - lurst 

* How you are sure about "clean" environments/ package deps etc ? - ebal[m]  

* How realistic is 100% in Core and Extra? And, what is the minimum percentage you are aiming for across all repos - steamwreck

* Which terminal & shell is this? - orhun

* Are there security implications in making packages reproducible (i.e., losing randomisation in hashmaps, etc)? - s3krit
the reason for pythonhashseed being randomised by default was to prevent hash collision DoS attacks: is there another mitigation for that, or was it a tradeoff in favour for reproducable builds? - kgz

* When the build fails, do you chug the rest of the beer? - kattjevfel ;)

* Do these patch make any sense to provide upstream ? - ebal

* Do you have a way to provide exceptions to reproducabilty, for instance when you don't want to patch out timestamps? - abaumann

* Comparing large container files (e.g. installation images) using diffoscope is currently an issue (crashes)? How does this currently impact large packages (e.g. game data)? -dvzrv (David)

* what's the endgame for reproducable builds in arch? If a package starts being reproducable, can you block all new uploads that don't reproduce? - kgz
* Are there any guards that prevent a reproducible package that could not be reproduced from entiring the repo? - RamZ

* A few packages in Arch Linux are not built from source but just repackaged binaries from upstream. Do you think the packaging guidelines will change if reproducible builds become more important in the future as anthraxx (IIRC) has said? - stefan0xC

* Where can I see/get updates on if packages I maintain are reproducible?  - daurnimator

* So if one of your dependendencies or your source were polluted, how would you notice that. You build twice using the same polluted thing, and there: prefect match - u1106

* What are your hobbies other then arch linux and reproducible builds? - Bullsokk