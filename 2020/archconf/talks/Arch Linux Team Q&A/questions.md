# Arch Linux Team Q&A - Arch Linux core team

## Round-table:

* What do you do for fun (other than  Arch), such as sports etc? - dr_hashimoto

* What setups have you guys got (hardware)? I assume you all have your own enterprise class servers with the ability to compile pacakges in seconds / another one not super serious - rhysperry111

* Whats your favourite IDE/Editor setup? - lurst

* Vim or emacs - Skugfisk

* What is your DE/WM choice?  - thaewrapt

* If you could run Arch linux on every single eletronic device, which things would you choose NOT to run arch on? - lurst

* If you could run Arch linux on every single eletronic device, which things would you choose NOT to run arch on? -lurst

* How much money does arch need to run everything per year? - dr_hashimoto

* Do you all have [testing] enabled? - rotsix

* How would you convince people to use the [testing] repos? -ArchConf|59

* How do you feel about multi-version-maintenance (neglecting alternatives, just install to different paths), like Node.jswhich supports multiple stable releases at a time? -cyrevolt

## Technical:

* Do you think recent popularity and stabilization of btrfs will drive more people to rolling distros as "time shift" prevents impossible to boot/run scenarios? - Ilgaz

* What about kernel live patching and/or quick reboots with kexec? - post-factum

* Will you use metalink v4 like Fedora in future for downloads without having to test every mirror for speed - Ilgaz

* Did you notice the great increase in iso torrent download activity while the conference is running ? - Ilgaz

* Hurd support when? - tamz

* Do you guys want a gui for aur like a ubuntu store? - felixonmars

* Can the default shell switch to zsh? - seankhliao (users can change their shell /bin/sh != default user shell

* Is there any progress on the whole "modules break after kernel update" thing - rhysperry111

* Do you think recent popularity and stabilization of btrfs will drive more people to rolling distros as "time shift" prevents impossible toof btrfs will drive more people to rolling distros as "time shift" prevents impossible to boot/run scenarios? - Ilgaz


* What distros did you guys come from and why did you change? (Or was it Arch initially?) - macavityscat 

* What's the best learning lesson from contributing? - lurst

* Anything new on the seamless [reboot-free] kernel updates? - oak_

* Any plans to make the installation easier? -aaryan1x

* What's up with haskell packages? no lol fair play :P

* are there arch developers/trusted/users who use arch in a work/enterprise/company context? I use it for a pxe-booted rescue system on a few hundred machines but I kinda feel alone with this setup - demaio

* Why is there not a graphical installer on the ISO? - Ram-Z

* When will Arch get debug packages? - amcrae & Ram-Z

* Are there any plans to make Arch Linux available as WSL (Windows Subsystem for Linux) distribution? - ironborn

* If someone wants to be a contributor, what is the one (or many) places to join to contribute to Arch linux? - lurst

* How does one become a trusted user? - lurst

* What is the most fun or interesting part, working/being part of the Arch Linux Team? -bittin

* Is it worth becoming a TU if you want to bring 1 package only into the repos? - post-factum

* Is there any chance Arch will be doing google summer of code and outreachy in the future? -meskarune (jelle can answer)

* What's the most challenging thing they've done as part of contributing to Arch - lurst

* How much time are you devoting to arch development, and do you have day jobs related to programming/development? - Skugfisk

* What's the best learning lesson from contributing - lurst

* What drove you to join the arch linux team? - rhysperry111

* Why not adapt a Calamares based installer for basic desktop users, as an option? All work seems to have done by Manjaro team so it won't need by Manjaro team so it won't need too much time

* How often do you update your system? - ArchConf|59

* What is the first thing you do after getting a fresh Arch install up and running? - the_wizzrd

* If Arch had paid positions, what would be the first position/responsibility to use that salary on? - thurstylark

* What would you tell yourself if you went back before you started contributing (ignoring all the problems of changing the past) - lurst

* How do you guys test the package builds and manage to maintain that many packages? archroot-nspwan , qemu  / another vm thing? - ArchConf|84