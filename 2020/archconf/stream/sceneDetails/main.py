#!/usr/bin/python

# Copyright © 2020 Morten Linderud

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
# OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.



import requests
import pytz

from datetime import date, datetime, timedelta

SCHEDULE = "https://pretalx.com/arch-conf-online-2020/schedule/export/schedule.json"

# today = datetime.fromisoformat('2020-10-10T10:00:00+02:00')

today = datetime.now(tz=pytz.timezone('Europe/Oslo'))
print(today)
# print(today)


r = requests.get(SCHEDULE)
j = r.json()

conf = j['schedule']['conference']



def write_text_slides(
        title="",
        desc="",
        time="",
        time_left="",
        authors=[],
        ):
    with open("nextDesc.txt", "w") as f:
        if len(desc) >= 350:
            f.write(desc[:350]+"...")
        else:
            f.write(desc)
    with open("nextWho.txt", "w") as f:
        f.write(" & ".join(authors))
    with open("nextTime.txt", "w") as f:
        f.write(f"{time}\nin {time_left} min")
    with open("nextTitle.txt", "w") as f:
        if len(title) >= 88:
            f.write(title[:88]+"...")
        else:
            f.write(title)
    return


WRITE_TALK = {}

SCHEDULE = []

WRITTEN=False

for day in conf["days"]:
    if day["date"] != today.strftime("%Y-%m-%d"):
        continue
    for talk in day["rooms"]["Arch Conf"]:
        talk_date = datetime.fromisoformat(talk["date"])

        # Talk time has passed
        if (talk_date < today) and ((today - talk_date) >= timedelta(minutes=5)):
            continue

        sched_title = talk["title"]
        if len(sched_title) >= 60:
            sched_title = sched_title[:60]+"..."

        if WRITTEN:
            SCHEDULE.append(f"  {talk_date.strftime('%H:%M')} {sched_title}")
            continue

        SCHEDULE = [f"* {talk_date.strftime('%H:%M')} {sched_title}", *SCHEDULE]
        delta = talk_date-today
        min_left = (delta.seconds//60)
        if min_left > 1000:
            min_left = 0
        details = {"title": talk["title"],
                   "desc": talk["abstract"],
                   "time": talk["start"],
                   "time_left": min_left,
                   "authors": [name["public_name"] for name in talk["persons"]]}
        write_text_slides(**details)
        WRITTEN=True


# Breaks are not part of the schedule. But because we have it on a set time, we
# can just add it to the list.
for index, item in enumerate(SCHEDULE):
    if today.strftime('%H') in ("15", "16"):
        SCHEDULE = ["  16:00 DJ Set (Break)", *SCHEDULE]
        break
    if "15:00" not in item:
        continue
    SCHEDULE = [*SCHEDULE[:index+1],"  16:00 DJ Set (Break)", *SCHEDULE[index+1:]]
    break


with open("nextSchedule.txt", "w") as f:
    f.write("\n\n\n".join(SCHEDULE[:5]))
