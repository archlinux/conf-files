# distri: researching fast Linux package management - Micheal Stapelberg

* Are there any performance implications when mounting a large number of SquashFS images simultaneously? - diabonas, danielrparks

* Is there a tradeoff between complexity and simplicity when using distri? - Foxboron

* What are the differences between distri and Nix(OS)? Would it be possible to implement the performance improvements distri provides in Nix? - diabonas, u1106, cyrevolt

* Are images smaller in size compared to packages in zstd? - lieto

* How does distri perform in terms of storage overhead compared to other package managers? - thedjdoorn

* Is distri basically similar to Snaps or FlatPaks? For example, one image that contains everything and is independent of the system (sort of)? Does it suffer from the same security issues? - sateffen, rotsix

* Will pacman implement distri package manager's ways of doing things? - nucifrangibulum_

* Each package is stored with its dependencies, what about storage usage? - rotsix

* Are reproducible builds better to apply with this kind of installation system? - kieto

* How flexible is the packaging process? How much is the overhead cost with different scenarios like an electron desktop app and a simple command line app? - orhun

* How do you handle things like generating an initramfs that needs to be done after package installation on the user's system, conventionally using hooks? - diabonas

* How is the base system set up with this approach? Thinking of the filesystem package in Arch for example. If there are no dependencies how are base requirements as having a TTY guaranteed? -zvyn

* If Arch might not be the first choice, which distributions and use cases would qualify best then? -ArchConf|46

* What about a libc update? Isn't that a full reinstall or actually more than that, because there are many copies of libc? - u1106

* Imagine having two version of sshd installed. Can we have those two versions running simultaneously? Which version is prefered?  -rotsix

* How do you feel about large runtimes that could be shared? Just "don't care about space" (intentionally)? -cyrevolt

* In other package managers packages can have optional dependencies Do distri images include all optional dependencies or just the required ones? - thedjdoorn

* How does distri perform in terms of storage overhead compared to other package managers? - thedjoorn

* What about global configuration like stuff in /etc? - seankhliao

* Isn't resolving dependencies faster than redownloading deps for every package if they're going to be part of the images? - fredmorcos
 
* What is the runtime overhead for distri installed packages? Mount + FUSE +...? - ynikitenko

* Why push something to runtime when I can do it at installation time? Wouldn't this just slow the system down overall in the name of "simplicity"? - fredmorcos