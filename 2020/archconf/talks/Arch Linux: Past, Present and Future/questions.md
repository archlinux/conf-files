# Arch Linux: Past, Present and Future - Allan McRae, Levente 'antraxx' Polyak 

* What are "info pages"? - lurst

* Will there be another split in repos - marcthe12

* Where does the name 'arch linux' come from? - s3krit

* what i don't understand is svn choice? whyyy?.. - pfactum

* Will there be an official arm port if it gets popular like arm is becoming more popular - marcthe12

* How about taking Arch Linux ARM into the Arch Linux fold? - Solskogen

* Why is ALARM not more tightly integrated with Arch? - daurnimator

* Anyone interested in Arch RISC-V? - felixonmars

* How's the diversity of the people in charge of Arch? - lurst

* What about being more cloud friendly distro? - ArchConf|22

* Can arch use ci for packages? - marchthe12

* what's the current state of the discussion regarding dropping support for old CPUs? - malaco

* What sort of tools do you use on your servers? Such as Ansible, Gitlab CI/CD etc? - hash15

* There was a discussion a while ago about single repo vs one repo per package, what did you go for? - RamZ

* Where is Arch linux lacking for more volunteer power? - lurst

* What is Arch's relation with derivative projects (Parabola, Arix, Alarm, etc) - tamz
* Do you cooperate with other arch-based distros, or planning to do so? If yes: in which way? (Manjaro, Endevour)? - sateffen

* How does Arch plan on continually succeeding as a major distrobution going forward? - jstamant

* To me it seems Arch is really Europe-centered, is that true? - Nicemicro

* important question on arch bugs: is there some initiative to go through old and irrelevant ones and close them? - pfactum
