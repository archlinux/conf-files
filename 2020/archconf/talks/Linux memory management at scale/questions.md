# Linux memory management at scale - Chris Down

* Pros/cons of Swapfile vs Swap partition? - mackilanu

* How did you get into this area of kernel development/management? Like, what was your journey? - dr_hashimoto

* How much swap should be allocated? Does it have to be twice the RAM? - grijul

* I run all my production systems with `vm.overcommit_memory = 2`: why shouldn't I; or alternatively: why doesn't everyone else do this? - daurnimator

* Do you need to be using cgroup2 for workloads to make use of any of the PSI metrics?  - kgz

* Are cgroups something I should look into on a small homeserver (Raspberry pi 2 Model B) - rhysperry111

* Does facebook use arch or are you just a personal user of arch? - markmarkmark

* What do you think of earlyOOM - rhysperry111

* Do i really need the entire fbtax or oomd is enough? - oak_

* Any chance of updating the Archwiki to reflect this talk? - aithcy

* How do you feel about using zswap/zram for swap versus file based swap - jelle, Ilgaz

* How does the IO latency guarantee for slices work? Also what happens when that guarantee can't be met? - kgz

* How important is the relationship between system memory. page files, in memory file cache and similar technologies. And is there anything you would personally like to see more widely adopted? - MaIlchad

* This talk assumes that i know stuff about linux memory management. Where can i read about all this basic(?) stuff?  - oak_
